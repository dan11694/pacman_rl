# Pacman-AI
A repository for the Solutions for the PacMan assignment from Berkley.

The original assignment: https://inst.eecs.berkeley.edu/~cs188/sp22/projects/
Original repo (code) : https://github.com/Aveek-Saha/Pacman-AI

# Usage

To run the repo for yourself, clone it and follow the steps below:

Create a new conda env with python 3.6

```
conda create --name pacman python=3.6
conda activate pacman
```

Go inside reinforcement folder and run
```
python pacman.py -p ApproximateQAgent -a extractor=SimpleExtractor -x 50 -n 60 -l mediumClassic -g DirectionalGhost
```
Main parameters are: number of training episodes (x), number of total episodes (n), layout small-medium-originalClassic for different grid sizes (l)

Traces are saved as pickle files in savedGames/win or lose, depending on final outcome

ANOTHER INTERESTING SCENARIO: Multiagent with expectimax algorithm
```
python autograder.py -q q5
```
set parameters in multiagent/test_cases/q5/grade-agent.test
The agent always wins with one ghost on mediumClassic, but seems to stop and wait for ghosts in the end (to avoid facing them while exploring probably). Moreover, ghosts are eaten more often. 
Saving similar to before.


# ILASP search space
1. Write .las file for each action with mode bias
2. Run generate_ss.sh (set the list of actions inside)
3. The .las files with explicit search space will be action_cut.las
4. It is possible to define more constraints for cutting search space in the script cut_ss.py