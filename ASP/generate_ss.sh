ILASP="/usr/local/bin/ILASP"
PYTHON="/home/daniele/Documents/miniconda3/envs/pacman/bin/python3"

for A in "escape" "wait"; do
    $ILASP --version=4 -nc -ml=5 --max-rule-length=6 -s $A".las" > "search_"$A".txt"
done

$PYTHON cut_ss.py