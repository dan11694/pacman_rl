actions = ["wait", "escape"]
relevant_atoms = ["distance", "totalFood"]

for a in actions:
    lines_search = []
    with open("search_"+a+".txt", "r") as f:
        for l in f:
            good = True
            for atom in relevant_atoms:
                if atom in l:
                    where_atom = l.index(atom)
                    where_end_atom = l[where_atom:].index(")")
                    substring = l[where_atom : where_atom + where_end_atom].split(",")
                    relevant_var = substring[-1]
                    if relevant_var + ">" not in l.replace(" ", "") and relevant_var + "<" not in l.replace(" ", ""):
                        good = False
            
            if good:
                lines_search.append(l)
    
    lines_las = []
    with open(a+".las", "r") as f:
        for l in f:
            if "#modeb" not in l and "#modeh" not in l and "#maxv" not in l:
                lines_las.append(l)

    

    with open(a+"_cut.las", "w") as f:
        f.writelines(lines_las + ["\n\n\n\n\n\n\n\n\n\n"] + lines_search)
    
